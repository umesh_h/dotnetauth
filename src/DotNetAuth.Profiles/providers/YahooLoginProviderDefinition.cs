using Newtonsoft.Json.Linq;

namespace DotNetAuth.Profiles
{
    public class YahooLoginProviderDefinition : LoginProviderDefinition
    {
        readonly ProfileProperty[] supportedProperties;
        public YahooLoginProviderDefinition()
        {
            Name = "Yahoo";
            Fullname = "Yahoo";
            ProtocolType = ProtocolTypes.OAuth1a;
            supportedProperties = new[] { 
                ProfileProperty.UniqueID,
                ProfileProperty.BirthDate,
                ProfileProperty.LastName,
                ProfileProperty.Gender,
                ProfileProperty.FullName,
                ProfileProperty.Location,
                ProfileProperty.Locale,
                ProfileProperty.PictureLink,
                ProfileProperty.ProfileLink,
            };
        }
        public override ProfileProperty[] GetSupportedProperties()
        {
            return supportedProperties;
        }
        public override string GetProfileEndpoint(ProfileProperty[] requiredProperties)
        {
            return "http://query.yahooapis.com/v1/yql?q=select%20*%20from%20social.profile%20where%20guid%3Dme%3B&format=json";
        }
        public override string GetRequiredScope(ProfileProperty[] requiredProperties)
        {
            return "";
        }
        public override Profile ParseProfile(string content)
        {
            var json = JObject.Parse(content);
            var profileJson = json.Value<JObject>("query").Value<JObject>("results").Value<JObject>("profile");
            var profile = new Profile {
                UniqueID = profileJson.Value<string>("guid"),
                Email = profileJson.Value<JArray>("emails")[0].Value<string>("handle"),
                BirthDate = profileJson.Value<string>("birthdate"),
                LastName = profileJson.Value<string>("familyName"),
                Gender = profileJson.Value<string>("gender"),
                FirstName = profileJson.Value<string>("givenName"),
                Location = profileJson.Value<string>("location"),
                Locale = profileJson.Value<string>("lang"),
                PictureLink = profileJson.Value<JObject>("image").Value<string>("imageUrl"),
                ProfileLink = profileJson.Value<string>("profileUrl"),
                Timezone = profileJson.Value<string>("timeZone")
            };
            return profile;
        }
        public override OAuth1a.OAuth1aProviderDefinition GetOAuth1aDefinition()
        {
            return new OAuth1a.Providers.YahooOAuth1a();
        }
    }
}
